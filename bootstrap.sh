#!/bin/sh

[ -f accesslog.c ] || {
    echo $0: must be run from top of the source directory >&2
    exit 1
}

rm -f \
    install-sh \
    mkinstalldirs \
    missing \
    COPYING \
    INSTALL \
    depcomp

aclocal
# autoheader
automake -a
autoconf
