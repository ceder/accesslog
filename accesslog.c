/* Set up AccessLog permissions. */

#include <fcntl.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

static char *progname = NULL;

#define TERMINAL "AccessLog"

static char *valid_prefixes[] = {
  "/lysator/www/user-pages",
  "/lysator/www/roxen-pages",
};

static void
usage(void)
{
  fprintf(stderr, "usage: %s [ directory | file ]\n", progname);
  exit(1);
}

static void
my_perror(const char *msg)
{
  fprintf(stderr, "%s: ", progname);
  perror(msg);
  exit(1);
}

static void
descend_path(const char *in_path)
{
  struct stat link_stat;
  struct stat fd_stat;
  int fd;
  char *path;

  if ((path = strdup(in_path)) == NULL)
    my_perror("strdup");

  path = strtok(path, "/");

  while (path != NULL)
    {
      if (strcmp(path, TERMINAL) == 0)
	{
	  if (strtok(NULL, "/") != NULL)
	    {
	      fprintf(stderr, "%s: ``%s'' must be the last part of the path\n",
		      progname, TERMINAL);
	      exit(1);
	    }
	  break;
	}

      if (!strcmp(path, ".."))
	{
	  fprintf(stderr, "%s: ``..'' is not allowed\n", progname);
	  exit(1);
	}

      fd = open(path, O_RDONLY);
      if (fd < 0)
	my_perror(path);

      if (lstat(path, &link_stat) < 0)
	my_perror(path);

      if (fstat(fd, &fd_stat) < 0)
	my_perror("fstat");

      if (!(S_ISDIR(link_stat.st_mode) && S_ISDIR(fd_stat.st_mode)
	    && link_stat.st_ino == fd_stat.st_ino
	    && link_stat.st_dev == fd_stat.st_dev))
	{
	  fprintf(stderr, "%s: %s: security check failed\n", progname,
		  path);
	  exit(1);
	}

      fchdir(fd);
      close(fd);

      path = strtok(NULL, "/");
    }
}


static void
fix_permissions(void)
{
  struct stat link_stat;
  struct stat fd_stat;

  int fd = open(TERMINAL, O_WRONLY|O_CREAT, 0620);
  if (fd < 0)
    my_perror(TERMINAL);

  if (lstat(TERMINAL, &link_stat) < 0)
    my_perror(TERMINAL);

  if (fstat(fd, &fd_stat) < 0)
    my_perror("fstat");

  if (!(S_ISREG(link_stat.st_mode) && S_ISREG(fd_stat.st_mode)
	&& link_stat.st_ino == fd_stat.st_ino
	&& link_stat.st_dev == fd_stat.st_dev))
    {
      fprintf(stderr, "%s: %s: security check failed\n", progname,
	      TERMINAL);
      close(fd);
      exit(1);
    }

  if (fchown(fd, getuid(), getegid()) < 0)
    my_perror("chown");
  if (fchmod(fd, 0620) < 0)
    my_perror("chmod");

  close(fd);
}


  
static void
use_good_prefix(const char *prefix,
		int prefix_len,
		char *path)
{
  struct passwd *entry = getpwuid(getuid());
  int name_len;

  if (entry == NULL)
    {
      fprintf(stderr, "You don't exist. Go away.\n");
      exit(1);
    }

  name_len = strlen(entry->pw_name);
  if (strncmp(entry->pw_name, &path[prefix_len + 1], name_len) != 0
      || (path[prefix_len + 1 + name_len] != '\0'
	  && path[prefix_len + 1 + name_len] != '/'))
    {
      fprintf(stderr, "%s: you can only manipulate your own AccessLogs\n",
	      progname);
      exit(1);
    }

  if (chdir("/") < 0)
    my_perror("/");

  if (chdir(prefix) < 0)
    my_perror(prefix);

  descend_path(&path[prefix_len + 1]);
  
  fix_permissions();
}


  
int
main(int argc,
     char **argv)
{
  int ix;
  char *path;
      
  progname = argv[0];

  if (argc != 2)
    usage();

  path = argv[1];

  if (getuid() != geteuid())
    {
      fprintf(stderr,
	      "%s: installation problem: should be setgid, not setuid\n",
	      progname);
      exit(1);
    }

  for (ix = 0; ix < sizeof(valid_prefixes) / sizeof(valid_prefixes[0]); ++ix)
    {
      int len = strlen(valid_prefixes[ix]);

      if ((!strncmp(valid_prefixes[ix], path, len))
	  && len < strlen(path) && path[len] == '/')
	{
	  use_good_prefix(valid_prefixes[ix], len, path);
	  exit(0);
	}
    }

  /* Bad prefix given. */
  fprintf(stderr,
	  "%s: bad argument: must start with one of the following prefixes:\n",
	  progname);
  for (ix = 0; ix < sizeof(valid_prefixes) / sizeof(valid_prefixes[0]); ++ix)
    fprintf(stderr, "\t%s\n", valid_prefixes[ix]);
  exit(1);
}
